import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Принимает путь к файлу
 * Возвращает мапу отец->ребенок
 */

public class MapExecutor implements Callable<LinkedBlockingQueue<Reflection>> {
    private final LinkedBlockingQueue<Reflection> collector = new LinkedBlockingQueue<>();
    private final Path path;

    public MapExecutor(Path path) {
        this.path = path;

    }

    private LinkedList<String> findReg(String str, String name) {
        name = name.substring(0, name.length() - 5);
        Pattern pattern = Pattern.compile(
                "\\s" + name + "(.*([A-Z][a-zA-Z]*).*)" + "\\{");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            Pattern pattern1 = Pattern.compile("[A-Z][a-zA-Z]*");
            Matcher matcher1 = pattern1.matcher(str);
            LinkedList<String> list = new LinkedList<>();
            while (matcher1.find()) {
                String substring = str.substring(matcher1.start(), matcher1.end());
                if (!substring.equals(name)) {
                    list.add(substring);
                }
            }
            return list;
        } else
            return null;
    }

    private void processString(String str, String name) {
        LinkedList<String> reg = findReg(str, name);
        if (reg == null) {
            return;
        }
        for (String s : reg) {
            collector.add(new Reflection(s, name));
        }
    }

    public void run() {
        try {
            Files.lines(this.path).forEach(x -> processString(x, this.path.getFileName().toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public LinkedBlockingQueue<Reflection> call() {
        run();
        return new LinkedBlockingQueue<>(collector);
    }
}
