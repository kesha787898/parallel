import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.Callable;
/**
 * Принимает Сет Отношений
 * Возвращает Мапу Родитель->дети
 * */
public class ReduceExecutor implements Callable<HashMap<String, HashSet<String>>> {
    HashMap<String, HashSet<String>> result = new HashMap<>();
    private final HashSet<Reflection> inp;

    public ReduceExecutor(HashSet<Reflection> inp) {
        this.inp = inp;
    }

    public void run() {
        for (Reflection reflection : inp) {
            HashSet<String> value = result.getOrDefault(reflection.parent, new HashSet<>());
            value.add(reflection.child);
            result.put(reflection.parent, value);
        }
    }

    @Override
    public HashMap<String, HashSet<String>> call() {
        run();
        return result;
    }
}
