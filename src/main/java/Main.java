import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Main {
    /**
     * принимает очередь фьючерсов мап отец->ребенок
     * возвращает очередь списков мап отец->ребенок
     */
    static private BlockingQueue<HashSet<Reflection>> shuffle(BlockingQueue<Future<LinkedBlockingQueue<Reflection>>> futures) throws ExecutionException, InterruptedException {
        HashMap<String, HashSet<Reflection>> reflections = new HashMap<>();
        for (Future<LinkedBlockingQueue<Reflection>> future : futures) {
            LinkedBlockingQueue<Reflection> set = future.get();
            for (Reflection reflection : set) {
                HashSet<Reflection> refSet = reflections.getOrDefault(reflection.parent, new HashSet<>());
                refSet.add(reflection);
                reflections.put(reflection.parent, refSet);
            }
        }
        return new LinkedBlockingQueue<>(reflections.values());
    }

    public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
        List<Path> paths = Files.walk(Paths.get("src/main/java/pack"))
                .filter(Files::isRegularFile).collect(Collectors.toList());
        final int numThreads = 5;
        ExecutorService serviceMap = Executors.newFixedThreadPool(numThreads);
        BlockingQueue<Future<LinkedBlockingQueue<Reflection>>> futuresMap = new LinkedBlockingQueue<>();

        for (Path path : paths) {
            futuresMap.offer(serviceMap.submit(new MapExecutor(path)));
        }
        serviceMap.shutdown();
        BlockingQueue<HashSet<Reflection>> shuffle = shuffle(futuresMap);

        ExecutorService serviceReduce = Executors.newFixedThreadPool(shuffle.size());
        BlockingQueue<Future<HashMap<String, HashSet<String>>>> futuresReduce = new LinkedBlockingQueue<>();
        for (HashSet<Reflection> set : shuffle) {
            futuresReduce.offer(serviceReduce.submit(new ReduceExecutor(set)));
        }
        serviceReduce.shutdown();
        for (Future<HashMap<String, HashSet<String>>> hashMapFuture : futuresReduce) {
            System.out.println(hashMapFuture.get());
        }

    }
}
