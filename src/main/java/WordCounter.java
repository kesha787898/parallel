import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.function.Function;
import java.nio.file.Paths;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class WordCounter {
    public static void main(String[] args) throws IOException {
        File file = new File("input.txt");
        System.out.println(Arrays.toString(Files.lines(Paths.get(file.getAbsolutePath())).
                map(x -> Arrays.stream(x.split("\\W+")).
                        collect(groupingBy(Function.identity(), counting()))).toArray()));

    }
}
