/*package hadoop;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.Mapper;

public class MapClass extends Mapper<Object, Text, Text, Text> {

    private LinkedList<String> findReg(String str, String name) {
        name = name.substring(0, name.length() - 5);
        Pattern pattern = Pattern.compile(
                "\\s" + name + "(.*([A-Z][a-zA-Z]*).*)" + "\\{");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            Pattern pattern1 = Pattern.compile("[A-Z][a-zA-Z]*");
            Matcher matcher1 = pattern1.matcher(str);
            LinkedList<String> list = new LinkedList<>();
            while (matcher1.find()) {
                String substring = str.substring(matcher1.start(), matcher1.end());
                if (!substring.equals(name)) {
                    list.add(substring);
                }
            }
            return list;
        } else
            return null;
    }

    @Override
    protected void map(Object key, Text value,
                       Context context) {
        Text name =new Text(((FileSplit) context.getInputSplit()).getPath().getName());
        String x = value.toString();
        LinkedList<String> reg = findReg(value.toString(), name.toString());
        if (reg == null) {
            return;
        }
        for (String s : reg) {
            try {
                context.write(new Text(s), name);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}*/