/*package akka;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


public class AkkaMaster extends AbstractActor {
    ConcurrentLinkedQueue<ResultMessage> results = new ConcurrentLinkedQueue<>();

    AtomicInteger counter = new AtomicInteger(0);
    int len;

    enum end {add}

    private void processFiles(String pathToPackage) throws IOException {
        List<Path> paths = Files.walk(Paths.get(pathToPackage))
                .filter(Files::isRegularFile).collect(Collectors.toList());
        len = paths.size();
        for (Path path : paths) {
            ActorRef actorRef = getContext().actorOf(
                    AkkaMap.props());
            actorRef.tell(path.toString(), getSelf());
            actorRef.tell(PoisonPill.getInstance(), ActorRef.noSender());
            while (!actorRef.isTerminated()) {
            }
            counter.incrementAndGet();
        }
    }


    @Override
    public Receive createReceive() {
        return receiveBuilder().
                match(String.class, this::processFiles).
                match(ResultMessage.class, reflection -> {
                    results.add(reflection);
                    if (counter.get() == len) {
                        getSelf().tell(PoisonPill.getInstance(), getSelf());
                    }
                }).
                build();
    }

    @Override
    public void postStop() {
        HashMap<String, HashSet<String>> result = new HashMap<>();
        for (ResultMessage reflection : results) {
            HashSet<String> value = result.getOrDefault(reflection.getParent(), new HashSet<>());
            value.add(reflection.getChild());
            result.put(reflection.getParent(), value);
        }
        System.out.println(result);
        getContext().getSystem().terminate();
    }
}*/
