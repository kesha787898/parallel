/*package akka;

import akka.actor.AbstractActor;
import akka.actor.Props;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;


class ResultMessage {
    private String parent;
    private String child;

    @Override
    public String toString() {
        return getParent() + ' ' + getChild();
    }

    public ResultMessage(String parent, String child) {
        this.parent = parent;
        this.child = child;

    }

    public String getParent() {
        return parent;
    }

    public String getChild() {
        return child;
    }
}


class AkkaMap extends AbstractActor {

    private LinkedList<String> findReg(String str, String name) {
        Pattern pattern = Pattern.compile(
                "\\s" + name + "(.*([A-Z][a-zA-Z]*).*)" + "\\{");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            Pattern pattern1 = Pattern.compile("[A-Z][a-zA-Z]*");
            Matcher matcher1 = pattern1.matcher(str);
            LinkedList<String> list = new LinkedList<>();
            while (matcher1.find()) {
                String substring = str.substring(matcher1.start(), matcher1.end());
                if (!substring.equals(name)) {
                    list.add(substring);
                }
            }
            return list;
        } else
            return null;
    }

    public static Props props() {
        return Props.create(AkkaMap.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(
                        String.class,
                        message -> {
                            try {
                                String[] names = message.substring(0, message.length() - 5).split("/");
                                String name = names[names.length - 1];
                                Stream<String> lines = Files.lines(Path.of(message));
                                lines.map(p -> this.findReg(p, name)).
                                        filter(Objects::nonNull).flatMap(List::stream).
                                        forEach(x ->
                                                getSender().tell(new ResultMessage( x,name), getSelf())
                                        );
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        })
                .build();
    }
}
*/