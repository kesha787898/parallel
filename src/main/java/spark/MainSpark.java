package spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainSpark {
    private static LinkedList<String> findReg(String str, String name) {
        Pattern pattern = Pattern.compile(
                "\\s" + name + "(.*([A-Z][a-zA-Z]*).*)" + "\\{");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            Pattern pattern1 = Pattern.compile("[A-Z][a-zA-Z]*");
            Matcher matcher1 = pattern1.matcher(str);
            LinkedList<String> list = new LinkedList<>();
            while (matcher1.find()) {
                String substring = str.substring(matcher1.start(), matcher1.end());
                if (!substring.equals(name)) {
                    list.add(substring);
                }
            }
            return list;
        } else
            return new LinkedList<>();
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Usage: JavaWordCount <file>");
            System.exit(1);
        }
        SparkConf sparkConf = new SparkConf().setAppName("JavaWordCount").setMaster("local[2]").set("spark.executor.memory", "1g");
        JavaSparkContext ctx = new JavaSparkContext(sparkConf);
        ctx.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
        JavaPairRDD<String, String> stringStringJavaPairRDD = ctx.wholeTextFiles(args[0]);

        List<Tuple2<String, Iterable<String>>> collect = stringStringJavaPairRDD.flatMap(s -> {
            String[] names = s._1.substring(0, s._1.length() - 5).split("/");
            String name = names[names.length - 1];
            final LinkedList<String> reg = findReg(s._2, name);
            return reg.stream().map(str -> new Tuple2<>(str,name)).iterator();
        }).filter(Objects::nonNull).mapToPair(x -> x).groupByKey().collect();

        ctx.stop();
        System.out.println(collect);
    }
}

